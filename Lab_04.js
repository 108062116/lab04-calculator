//console.log("Hello!");

document.addEventListener("keydown" , func3);
function func3(event){
    var asc = event.which || event.keycode;
    if((asc >= 96 && asc <= 111) || asc == 13 || asc == 8 || asc == 67){
        if(asc >= 96 && asc <= 105){
            asc = asc - 96;
            func1(asc);
        }else if(asc == 106){
            func1('*');
        }else if(asc == 107){
            func1('+');
        }else if(asc == 13){
            func1('=');
        }else if(asc == 109){
            func1('-');
        }else if(asc == 110){
            func1('.');
        }else if(asc == 111){
            func1('/');
        }else if(asc == 8){
            func1('k');
        }else if(asc == 67){
            func1('C');
        }
    }
}


function func1(a){
    var text = document.getElementById("screen");
    if(a != '='){
        if(a == 'C'){
            text.value = "0";
        }else if(a == 'k'){
            text.value = text.value.substring(0,text.value.length-1);
        }else{
            if(text.value != '0' && text.value != "error"){
                text.value = text.value + a;
            }else text.value = a;
        }
    }else{
        try{
            var t = text.value.indexOf('\0');
            if(t == -1){
                var ans = eval(text.value);
                text.value = ans;
            }else text.value = 'error';
        }catch(error){
            text.value = "error";
        }
    }
}


